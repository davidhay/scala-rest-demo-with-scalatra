package com.ealanta.demo

import org.scalatra._
import scalate.ScalateSupport
import com.ealanta.demo.repo._
import scala.tools.cmd.Opt.Implicit
import org.json4s._
import org.json4s.JsonDSL._
import com.fasterxml.jackson.annotation.JsonValue
import org.scalatra.json._

/**
 * @author davidhay
 */
class RestApp(implicit val repo: UserRepository, implicit val taskRepo: TaskRepository) extends 
  RestdemoStack with UrlGeneratorSupport with JacksonJsonSupport {

  implicit val jsonFormats = DefaultFormats

  get("/") {
    <html>
      <body>
        <h1>Hello, world!</h1>
        Say<a href="hello-scalate">hello to Scalate</a>
        .
      </body>
    </html>
  }

  get("/rest/users/?") {
    val usersJson = Extraction.decompose(repo.getUserList());
    val users: JValue = ("users", usersJson)
    users
  }

  val getUser: Route = get("/rest/users/:userid") {
    val userid = params.getAsOrElse[Int]("userid", halt(400))
    repo.getUserList().find { _.id == userid } match {
      case Some(user) => Ok(Extraction.decompose(user))
      case _          => NotFound(s"Sorry the user with id ${userid} cannot be found")
    }
  }

  post("/rest/users/?") {
    val user = parsedBody.extract[User]
    val id = repo.insert(user)
    val locationUrl = url(getUser, ("userid", id.toString))
    val headers = Map("Location" -> locationUrl)
    Created(headers = headers)
  }

  val getUserTasks: Route = get("/rest/users/:userid/tasks/?") {
    val id = params.getAsOrElse[Int]("userid", halt(400))
    val optUser = repo.getUser(id)

    optUser match {
      case Some(user) => {
        val tasks = taskRepo.getTaskList(user.id)
        val tasksJ = Extraction.decompose(tasks);
        val wrappedJ: JValue = ("tasks", tasksJ)
        wrappedJ
      }
      case _ => NotFound(s"Sorry the user with id ${id} cannot be found")
    }

  }

  val getUserTask: Route = get("/rest/users/:userId/tasks/:taskId") {
    val userId = params.getAsOrElse[Int]("userId", halt(400))
    val taskId = params.getAsOrElse[Int]("taskId", halt(400))
    val optUser = repo.getUser(userId)

    //If I didn't case about the different error messages, this would be simpler.

    optUser match {
      case Some(user) => {
        val tasks = taskRepo.getTaskList(user.id)
        tasks.find(_.id == taskId) match {
          case Some(task) => {
            val taskJ = Extraction.decompose(task);
            taskJ
          }
          case _ => NotFound(s"Sorry the task with id ${taskId} for user with id ${userId} cannot be found")
        }
      }
      case _ => NotFound(s"Sorry the user with id ${userId} cannot be found")
    }

  }

  post("/rest/users/:userId/tasks/?") {
    val userId = params.getAsOrElse[Int]("userId", halt(400))
    
    val optUser = repo.getUser(userId)

    optUser match {
      case Some(user) => {
        val task = parsedBody.extract[Task].copy(userId = userId)
        val taskId = taskRepo.insert(task)
        val locationUrl = url(getUserTask, ("userId", userId.toString), ("taskId", taskId.toString))
        val headers = Map("Location" -> locationUrl)
        Created(headers = headers)
      }
      case _ => NotFound(s"Sorry the user with id ${userId} cannot be found")
    }

  }

}
