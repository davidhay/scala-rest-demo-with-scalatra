package com.ealanta.demo.repo

import java.util.Date
case class Task(id:Int=0,description:String,completed:Boolean,dueDate:Date,userId:Int=0)
case class User(username:String, first:String, last:String, id: Int = 0)

/**
 * @author davidhay
 */
trait SlickTables { 

  val db:DbInfo
  
  import db.DateMapper._
  import db.driver.simple._

  class UserTable(tag: Tag) extends Table[User](tag, "person") {

    def id: Column[Int] = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def username: Column[String] = column[String]("username", O.NotNull)
    def first: Column[String] = column[String]("first", O.NotNull)
    def last:  Column[String] = column[String]("last", O.NotNull)
    def * = (username, first, last, id) <> (User.tupled, User.unapply)
  }

  val userTable = TableQuery[UserTable]

  class TaskTable(tag: Tag) extends Table[Task](tag, "person_task") {

    def id: Column[Int] = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def description: Column[String] = column[String]("description", O.NotNull)
    def completed: Column[Boolean] = column[Boolean]("completed", O.NotNull)
    def dueDate:  Column[Date] = column[Date]("due_date", O.NotNull)
    def user_id:  Column[Int] = column[Int]("user_id", O.NotNull)
    def * = (id, description, completed, dueDate, user_id) <> (Task.tupled, Task.unapply)
    def user = foreignKey("user_fk", user_id, userTable)(_.id, onUpdate=ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Cascade)

  }

  val taskTable = TableQuery[TaskTable]
  
}




