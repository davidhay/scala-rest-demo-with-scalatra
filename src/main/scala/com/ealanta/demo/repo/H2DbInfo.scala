package com.ealanta.demo.repo

import scala.slick.driver.H2Driver

import com.typesafe.config.ConfigFactory
/**
 * @author davidhay
 * There seems to be a problem with INSERTS using the H2 in memory database
 */
trait H2DbInfo extends DbInfo {

  val driver = H2Driver

  import driver.simple._

  def dbObject(): Database = 
      Database.forURL("jdbc:h2:mem:test", driver = "org.h2.Driver")

}

/**
 * @author davidhay
 */
class LoadingH2DBInfo extends H2DbInfo {

  import driver.simple._

  private val config = ConfigFactory.load()

  private val dbDriver = config.getString("db.driver")
  private val url = config.getString("db.url")

  //although we return a single database object, the scripts are run per connection which is not what I expected initially.
  override lazy val dbObject: Database = {
    Database.forURL(url, driver = dbDriver)
  }
}

/**
 * @author davidhay
 */
class LoadingH2DbInfoConfig extends DbInfoConfig {
  def dbInfo:DbInfo = new LoadingH2DBInfo
}

