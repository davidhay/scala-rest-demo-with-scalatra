package com.ealanta.demo.repo

/**
 * @author davidhay
 */
trait BaseSlickUserRepository extends UserRepository with SlickTables {

  import db.DateMapper._
  import db.driver.simple._

  def getUserList(): List[User] = {
    db.dbObject.withSession { implicit session: Session => userTable.list }
  }
  
  def getUser(userId:Int):Option[User] = {
    db.dbObject.withSession { implicit session: Session =>
      userTable.list.find(u=>u.id == userId)
    }
  }
  
  def insert(user: User): Int = {
    db.dbObject.withSession { implicit session: Session =>
      userTable.returning(userTable.map(_.id)).insert(user)
    }
  }

  def update(user: User): Int = {
    db.dbObject.withSession { implicit session: Session =>
      userTable.filter { _.id === user.id }.update(user)
    }
  }

  def delete(id: Int): Int = {
    db.dbObject.withSession { implicit session: Session =>
      userTable.filter { _.id === id }.delete
    }
  }

}

/**
 * @author davidhay
 */
class SlickUserRepository(implicit val config:DbInfoConfig) extends BaseSlickUserRepository {
   val db = config.dbInfo
}


