package com.ealanta.demo.repo;

import scala.slick.driver.MySQLDriver

/**
 * @author davidhay
 */
trait MySqlDbInfo extends DbInfo {

  val driver = MySQLDriver

  import driver.simple._

  val dbObject: Database = 
      Database.forURL(url="jdbc:mysql://127.0.0.1:3306/restdemo", driver = "com.mysql.jdbc.Driver", user="XXXX", password="XXXXXX")

}


/**
 * @author davidhay
 */
class MySqlDbInfoConfig extends DbInfoConfig {
  def dbInfo:DbInfo = new MySqlDbInfo { }
}





