package com.ealanta.demo.repo

/**
 * @author davidhay
 */
trait UserRepository  {

  def getUserList(): List[User]
  
  def insert(user: User): Int 

  def update(user: User): Int 

  def delete(id: Int): Int

  def getUser(userId:Int):Option[User]
  
}


