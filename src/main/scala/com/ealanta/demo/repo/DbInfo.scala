package com.ealanta.demo.repo

import scala.slick.driver.JdbcProfile

trait DbInfo {
  
  val driver: JdbcProfile
  
  def dbObject(): driver.simple.Database

  object DateMapper {
    import driver.simple._
    implicit val util2sqlDateMapper = MappedColumnType.base[java.util.Date, java.sql.Date](
      { utilDate => new java.sql.Date(utilDate.getTime()) },
      { sqlDate => new java.util.Date(sqlDate.getTime()) })
  }

}

trait DbInfoConfig {
  def dbInfo:DbInfo
}