package com.ealanta.demo.repo

/**
 * @author davidhay
 */
trait BaseSlickTaskRepository extends TaskRepository with SlickTables {

  import db.DateMapper._
  import db.driver.simple._

  def getTaskList(): List[Task] = {
    db.dbObject.withSession { implicit session: Session => taskTable.list }
  }

  def getTaskList(userId:Int): List[Task] = {
    db.dbObject.withSession { implicit session: Session => taskTable.filter{ _.user_id === userId }.list }
  }
  
  def insert(task:Task): Int = {
    db.dbObject.withSession { implicit session: Session =>
      taskTable.returning(taskTable.map(_.id)).insert(task)
    }
  }

  def update(task: Task): Int = {
    db.dbObject.withSession { implicit session: Session =>
      taskTable.filter { _.id === task.id }.update(task)
    }
  }

  def delete(id: Int): Int = {
    db.dbObject.withSession { implicit session: Session =>
      taskTable.filter { _.id === id }.delete
    }
  }

}

/**
 * @author davidhay
 */
class SlickTaskRepository(implicit val config:DbInfoConfig) extends BaseSlickTaskRepository {
   val db = config.dbInfo
}



