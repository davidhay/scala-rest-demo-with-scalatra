package com.ealanta.demo.repo

import scala.slick.driver.HsqldbDriver

import com.typesafe.config.ConfigFactory

/**
 * @author davidhay
 */
trait HSQLDbInfo extends DbInfo {

  val driver = HsqldbDriver

  import driver.simple._

  def dbObject(): Database = 
      Database.forURL("jdbc:hsqldb:mem:restdata", driver = "org.hsqldb.jdbcDriver", user="SA", password="")

}

/**
 * @author davidhay
 */
class HSQLDbInfoConfig extends DbInfoConfig {
  def dbInfo:DbInfo = new HSQLDbInfo {}
}

