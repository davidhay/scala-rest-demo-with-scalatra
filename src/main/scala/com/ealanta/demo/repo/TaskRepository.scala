package com.ealanta.demo.repo

/**
 * @author davidhay
 */
trait TaskRepository {

  def getTaskList(): List[Task];

  def getTaskList(userId: Int): List[Task]

  def insert(task: Task): Int

  def update(task: Task): Int

  def delete(taskId: Int): Int

}


