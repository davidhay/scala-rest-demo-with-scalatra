import org.scalatra._

import com.ealanta.demo._
import com.ealanta.demo.repo.DbInfoConfig
import com.ealanta.demo.repo.SlickUserRepository

import javax.servlet.ServletContext

/**
 * @author davidhay
 */
class ScalatraBootstrap extends LifeCycle {
  
  override def init(context: ServletContext) {
    import ScalatraBootstrap._
    context.mount(new RestApp, "/*")
  }
}

/**
 * @author davidhay
 * Currently setup with H2 in memory database.
 * Works for reads, but writes to db are not working. They do work for MySql though.
 */
object ScalatraBootstrap {

  import com.ealanta.demo.repo._
  
  //Db Inserts do work with MySQL
  //implicit val dbInfoConfig = new MySqlDbInfoConfig
  
  //Db Inserts are not working with H2 database
  implicit val dbInfoConfig = new LoadingH2DbInfoConfig
  
  implicit val userRepo:UserRepository = new SlickUserRepository
  implicit val taskRepo:TaskRepository = new SlickTaskRepository

}
