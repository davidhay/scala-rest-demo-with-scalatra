
CREATE TABLE IF NOT EXISTS `person`( 
  `id` integer NOT NULL AUTO_INCREMENT,
  `username` character varying(254) NOT NULL,
  `first` character varying(254) NOT NULL,
  `last` character varying(254) NOT NULL,
  CONSTRAINT user_pkey PRIMARY KEY (id) 
);


CREATE TABLE IF NOT EXISTS `person_task`( 
  `id` integer NOT NULL AUTO_INCREMENT,
  `description` character varying(254) NOT NULL,
  `due_date` date NOT NULL,
  `completed` boolean NOT NULL,
  `user_id` integer NOT NULL,
  CONSTRAINT task_pkey PRIMARY KEY (id),
  CONSTRAINT user_fk FOREIGN KEY (user_id) REFERENCES person (id) ON DELETE CASCADE
);

