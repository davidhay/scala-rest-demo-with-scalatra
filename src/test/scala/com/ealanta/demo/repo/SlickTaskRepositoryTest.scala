package com.ealanta.demo.repo

import org.scalatest.FunSuite
import org.junit.Ignore

/**
 * @author davidhay
 */
class SlickTaskRepositoryTest extends FunSuite  {

  implicit val dbInfoConfig = new LoadingH2DbInfoConfig
  val repo:TaskRepository = new SlickTaskRepository
  

  val $format = new java.text.SimpleDateFormat("dd-MM-yyyy")
  
  def format(date:java.util.Date) = $format.format(date);
  
  test("Get all tasks") {
    val tasks = repo.getTaskList();
    tasks.zipWithIndex.map {case (u,i) => println(s"[${i}][${u}]")}
    assert(tasks.length === 5)
  }

  test("Get all tasks for batman") {
    val tasks = repo.getTaskList(1);
    tasks.zipWithIndex.map {case (u,i) => println(s"[${i}][${u}]")}
    assert(tasks.length === 2)
  }

  test("Get task about batcave") {
   val task = repo.getTaskList(1).filter { t => t.description.toLowerCase().contains("batcave") }.head
   assert(task.description === "clean batcave")
   assert(task.completed === false)
   assert(format(task.dueDate) === "31-12-2014")
  }

  test("Add new Task ") {
    val task = Task(description="shopping",completed=false,dueDate=new java.util.Date,userId=1)
    assert(repo.insert(task) === 32)
  }

  test("Update Task") {
    val tasks = repo.getTaskList(1).filter { t => t.description.toLowerCase().contains("joker") }
    assert(tasks.size === 1,"cannot find task to update")
    val task = tasks.head
    val id = task.id
    val taskU = task.copy(description="get penguin and joker")
    val updated = repo.update(taskU)
    assert(updated === 1)
  }

  test("Delete Task") {
    val tasks = repo.getTaskList(2).filter { t => t.id == 22}
    assert(tasks.length == 1)
    assert(repo.delete(22) === 1)
  }

}
