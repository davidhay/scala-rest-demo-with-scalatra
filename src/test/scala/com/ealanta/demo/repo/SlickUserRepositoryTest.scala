package com.ealanta.demo.repo

import org.scalatest.FunSuite

/**
 * @author davidhay
 */
class SlickUserRepositoryTest extends FunSuite  {

  implicit val dbInfoConfig = new LoadingH2DbInfoConfig
  val repo = new SlickUserRepository
  
  test("Get all user info") {
    val users = repo.getUserList 
    users.zipWithIndex.map {case (u,i) => println(s"[${i}][${u}]")}
    assert(users.length === 3)
  }

  test("Add new User info") {
    val before = repo.getUserList().size
    val user = User("joebloggs", "Joe", "Bloggs")
    assert(repo.insert(user) === 4)
  }

  test("Update User info") {
    val updatedKnol = User("darknight", "bruce", "wayne", 1)
    assert(repo.update(updatedKnol) === 1)
  }

  test("Delete User info") {
    assert(repo.delete(3) === 1, "deleting user")
  }

}