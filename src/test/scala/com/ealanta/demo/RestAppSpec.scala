package com.ealanta.demo

import org.scalatra.test.specs2._
import com.ealanta.demo.repo.UserRepository
import com.ealanta.demo.repo.LoadingH2DbInfoConfig
import com.ealanta.demo.repo.SlickUserRepository
import com.ealanta.demo.repo.SlickTaskRepository
import org.specs2.matcher.JsonMatchers
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.specs2.matcher.MatchResult
import org.mockito.internal.matchers.EndsWith

/**
 * @author davidhay
 */
class RestAppSpec extends ScalatraSpec with JsonMatchers  { def is =
  
  """GET /rest/users on RestApp          
    should return status 200
    and 3 users             """          ! checkRestUsers ^
  """GET /rest/users/1 on RestApp          
    should return status 200
    and batman user             """      ! checkRestUser ^
  """GET /rest/users/1/tasks on RestApp          
    should return status 200
    and 2 tasks             """          ! checkRestUserTasks ^
  """GET /rest/users/1/tasks/10 on RestApp          
    should return status 200
    and 1 tasks             """          ! checkRestUserTask ^
  """GET /rest/users/1/tasks/999 on RestApp          
    should return status 404
    and a helpful error message             """          ! checkRestUserTaskWithUnknownTask ^
  """GET /rest/users/999/tasks/999 on RestApp          
    should return status 404
    and a helpful error message             """          ! checkRestUserTaskWithUnknownUser ^
  """GET /rest/users/999 on RestApp          
    should return status 404
    and a helpful error message             """          ! checkRestUserWithUnknownUser  ^
  """GET /rest/users/XXX on RestApp          
    should return status 400                """          ! checkRestUserWithBadUserId ^
  """POST /rest/users on RestApp          
    should return 201 with a Location header"""          ! checkCreateUser ^
  """POST /rest/users/1/tasks on RestApp          
    should return 201 with a Location header"""          ! checkCreateUserTask 
    end

  val restApp = {
    implicit val dbInfoConfig = new LoadingH2DbInfoConfig
    implicit val userRepo = new SlickUserRepository
    implicit val tastRepo = new SlickTaskRepository
    new RestApp
  }
                                                             
  addServlet(restApp, "/*")

  def checkRestUsers = get("/rest/users") {
    val resultJson = body
    
    val m1 = header("Content-Type") must startWith("application/json;")
    val m2 = resultJson must / ("users") /#0 /("username" -> "batman")
    val m3 = resultJson must / ("users") /#1 /("username" -> "spiderman")
    val m4 = resultJson must / ("users") /#2 /("username" -> "thehulk")
    
    val jvalue = parse(resultJson)
    val m5 = (jvalue \\ "users").children.length must_== 3
    val m6 = status must_== 200
    
    List(m1,m2,m3,m4,m5,m6).reduce(_ and _)

  }

  def checkRestUser = get("/rest/users/1") {
    val resultJson = body
    val jvalue = parse(resultJson)
    
    val m1 = header("Content-Type") must startWith("application/json;")
    val m2 = resultJson must / ("username" -> "batman")
    val m3 = resultJson must / ("first" -> "bruce")
    val m4 = resultJson must / ("last" -> "wayne")
    val m5 = resultJson must / ("id" -> 1)
    val m6 = status must_== 200
    
    List(m1,m2,m3,m4,m5,m6).reduce(_ and _)
  }
  
  def checkRestUserTasks = get("/rest/users/1/tasks") {
    val json = body
    
    val t1 = /("tasks") /# 0
    val t2 = /("tasks") /# 1
    val jvalue = parse(json)

    val m0  = header("Content-Type") must startWith("application/json;")
    val m1 = json must  t1 /("id"->10) 
    val m2 = json must  t1 /("description"->"get joker") 
    val m3 = json must  t1 /("completed"->false) 
    val m4 = json must  t1 /("dueDate"->"2014-12-31T00:00:00Z") 
    val m5 = json must  t1 /("userId"->1) 

    val m6  = json must  t2 /("id"->11) 
    val m7  = json must  t2 /("description"->"clean batcave") 
    val m8  = json must  t2 /("completed"->false) 
    val m9  = json must  t2 /("dueDate"->"2014-12-31T00:00:00Z") 
    val m10 = json must  t2 /("userId"->1) 

    val m11 = (jvalue \\ "tasks").children.size must_==2
    val m12 = status must_== 200
    
    List(m0,m1,m2,m3,m4,m5,m6,m7,m7,m8,m9,m10,m11).reduce(_ and _)
  }

  def checkRestUserTask = get("/rest/users/1/tasks/10") {
    val json = body
    val m0 = header("Content-Type") must startWith("application/json;")
    
    val m1 = json must /("id"->10) 
    val m2 = json must /("description"->"get joker") 
    val m3 = json must /("completed"->false) 
    val m4 = json must /("dueDate"->"2014-12-31T00:00:00Z") 
    val m5 = json must /("userId"->1) 
    
    val jvalue = parse(json)
    val m6 = status must_== 200
    
    List(m0,m1,m2,m3,m4,m5,m6).reduce(_ and _)
  }

  def checkRestUserWithUnknownUser = get("/rest/users/999") {
    val m1  = header("Content-Type") must startWith("text/html;")
    val m2  = status must_== 404
    val m3 = body must_== "Sorry the user with id 999 cannot be found" 
    m1 and m2 and m3
  }

  def checkRestUserWithBadUserId = get("/rest/users/XXX") {
    val m1  = header("Content-Type") must startWith("text/html;")
    val m2  = status must_== 400
    val m3 = body must_== "" 
    m1 and m2 and m3
  }

  def checkRestUserTaskWithUnknownTask = get("/rest/users/1/tasks/999") {
    val m1 = header("Content-Type") must startWith("text/html;")
    val m2 = body must_== "Sorry the task with id 999 for user with id 1 cannot be found"    
    val m3 = status must_== 404
    m1 and m2 and m3
  }

  def checkRestUserTaskWithUnknownUser = get("/rest/users/999/tasks/999") {
    val m1  = header("Content-Type") must startWith("text/html;")
    val m2  = status must_== 404
    val m3 = body must_== "Sorry the user with id 999 cannot be found" 
    m1 and m2 and m3
  }
  
  val contentTypeJson = Map("Content-Type" -> "application/json")
  
  def checkCreateUser = {
    val newUser = """ {"username":"davidhay","first":"david","last":"hay"} """
    post("/rest/users",body=newUser,headers = contentTypeJson){
      val m1 = status must_==201
      val m2 = header("Location") must startWith("/rest/users/")
      m1 and m2
    }
  }

  def checkCreateUserTask = {
    val newUserTask = """ {"description":"DESCRIPTION","completed":false,"dueDate":"2014-12-31T00:00:00Z"} """
    post("/rest/users/1/tasks",body=newUserTask,headers = contentTypeJson){
      println("================"+body)
      val m1 = status must_==201
      val m2 = header("Location") must startWith("/rest/users/1/tasks/")
      m1 and m2
    }
  }
  
}
