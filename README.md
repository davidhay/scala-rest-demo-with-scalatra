# Scala Rest Demo using Scala, Scalatra and Slick#
This Scala Application creates a Rest API for Users and their Tasks. This app uses Scalatra and Slick. It's my first Scalatra app.

This REST API does not support Hypermedia. This REST API does not currently support updates or deletes.

The scala app uses ScalaTest to test the Data Repositories. It also uses Specs2 to test the Rest Endpoints.

There is a current problem with the scala app using the H2 in memory database, the database inserts are currently not being commited correctly.
The commits work okay when using MySQL, so the problem must be H2 specific.


## REST URLS FOR READ/GET  ##
* http://localhost:8080/users
* http://localhost:8080/users/{userid}
* http://localhost:8080/users/{userid}/tasks
* http://localhost:8080/users/{userid}/tasks/{taskid}
 
The scala app performs content-negotiation on GET responses. It returns JSON or XML data as requested.

```
#!python

curl -H "Accept: application/xml" http://localhost:8080/rest/users/1

<?xml version='1.0' encoding='UTF-8'?>

<resp><username>batman</username><first>bruce</first><last>wayne</last><id>1</id></resp>
```


## REST URLS FOR CREATE/POST ##

To create a new user 

post to http://localhost:8080/rest/users

```
#!python

curl -i -X POST -H "Content-Type: application/json" -d '{"username":"davidhay","first":"david","last":"hay"}' http://localhost:8080/rest/users

HTTP/1.1 201 Created
Content-Type: application/json; charset=UTF-8
Location: /rest/users/4
Content-Length: 0
Server: Jetty(9.1.5.v20140505)
```


To create a new task

post to http://localhost:8080/rest/users/{userid}/tasks


```
#!python

curl -i -X POST -H "Content-Type: application/json" \
-d '{"description":"DESCRIPTION","completed":false,"dueDate":"2014-12-31T00:00:00Z"}' http://localhost:8080/rest/users/1/tasks
HTTP/1.1 201 Created
Content-Type: application/json; charset=UTF-8
Location: /rest/users/1/tasks/32
Content-Length: 0
Server: Jetty(9.1.5.v20140505)
```


To run the scala app

```
#!python

$ cd restdemo
$ ./sbt
> container:start
```